function fire(){
    this.vx += 20;
    this.fired = true;
}
function update(){
    if(this.fired == true){
        this.y -= 2;
        this.x += 2;
    }
}
function setCoordinates(x, y, vx, vy){
    this.x = x;
    this.y = y;
    this.vx = vx;
    this.vy = vy;
}
function missile(){
    this.fire = fire;
    this.fired = false;
}
var bomb = new missile();