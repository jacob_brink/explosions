var renderer = PIXI.autoDetectRenderer(720, 720);
var online_players = 0;
document.body.appendChild(renderer.view);
var stage = new PIXI.Container();
PIXI.loader.on("progress", loadProgressHandler).add("sprites/sprite_sheet.png").add("sprites/tilesheet.png").load(setup);

var ships = new Array(100);
var ships_names = new Array(100);

function loadProgressHandler(loader, resource) {
    
    console.log("loading: " + resource.url);
    console.log("progress: " + loader.progress + "%");
    
}
var foreign_rockets = new Array(100);

var o = 0;
var r;
var missiles = new missile();
var dirt = new Array(100);
var space_ship = new PIXI.Container();
//var platform = new platform(10, 10, 64, 64);
//platform.set();
//var fires;
function setup() {
    var texture = PIXI.utils.TextureCache["sprites/sprite_sheet.png"];
    var rocket_rectangle = new PIXI.Rectangle(96, 64, 32, 32);
    texture.frame = rocket_rectangle;
    
    while(o < 100){
        ships[o] = new PIXI.Graphics();
        ships[o].beginFill(0x9966FF);
        ships[o].drawCircle(0, 0, 32);
        ships[o].endFill();
        stage.addChild(ships[o]);
        o += 1;
    }
    
    r = new PIXI.Sprite(texture);
    r.x = 32;
    r.y = 0;
    stage.addChild(r);
    
    var otexture = PIXI.utils.TextureCache["sprites/tilesheet.png"];
    
    var dirt_rectangle = new PIXI.Rectangle(0, 160, 32, 32);
    otexture.frame = dirt_rectangle;
    
    
    //fires = new PIXI.Sprite(resources["Images/rocket_exhaust.png"]);
    //fires.x = 32;
    //fires.y = 32;
    
    
    var i = 0;
    while(i < 100){
        dirt[i] = new PIXI.Sprite(otexture);
        stage.addChild(dirt[i]);
        dirt[i].x = i * 32;
        i += 1;
    }
    

    
var left = keyboard(37);
var up = keyboard(38);
var right = keyboard(39);
var fire = keyboard(13);
var shoot = keyboard(32);
var toggle_pause = keyboard(27);
var pause = false;
toggle_pause.release = function(){
    if(pause == true){
        pause = false;
    } else {
        pause = true;
    }
}
    
shoot.press = function(){
    local_rocket.shoot = true;
    bomb.setCoordinates(local_rocket.x, local_rocket.y, local_rocket.vx, local_rocket.vy);
    bomb.fire();
}
shoot.release = function(){
    local_rocket.shoot = false;
}

fire.press = function(){
    local_rocket.fly = true;
};
fire.release = function(){
    local_rocket.fly = false;
}

left.press = function(){
    
    local_rocket.left = true;
    
};
left.release = function(){
    local_rocket.left = false;
}
right.press = function(){
    local_rocket.right = true;
};
  
right.release = function(){
    local_rocket.right = false;
}
up.press = function(){
    local_rocket.up = true;
}
up.release = function(){
    local_rocket.up = false;
}

r.anchor.x = .5;
r.anchor.y = .5;
r.rotation = local_rocket.angle;

 r.x = local_rocket.x;
 r.y = local_rocket.y;
    i = 0;
    var j = 0;
    var platform_block = 0;
while(j <= 2){
    if(j == 0){
        while((i < platform[j].amount_of_blocks)){//platform.amount_of_blocks){
        
        dirt[i].x = platform[j].blockX[i];
        dirt[i].y = platform[j].blockY[i];
        i += 1;
        }
    } else {
        
        var c = 0;
        var limit = 0;
        while(c <= j){
            limit += platform[c].amount_of_blocks;
            c += 1;
        }
        platform_block = 0;
        console.log("Limit: " + limit);
        while((i < limit)){
        
        console.log("I = " + i);
        console.log("J = " + j + " and X = " + platform[j].blockX[platform_block]);
        dirt[i].x = platform[j].blockX[platform_block];
        dirt[i].y = platform[j].blockY[platform_block];
        i += 1;
            platform_block += 1;
        }
    }
     
    
    j += 1;
}


}



function loop(){
    
    requestAnimationFrame(loop);
    local_rocket.update();
    r.vx = local_rocket.vx;
    r.vy = local_rocket.vy;
    r.x += r.vx;
    r.y += r.vy;
    r.rotation = local_rocket.angle;
    
    
    socket.on(document.cookie, function(data){
         console.log("data.message");
    });
    socket.emit("player_update", {'message': document.cookie + ":" + r.x + ":" + r.y + ":" + local_rocket.angle});
    
    renderer.render(stage);
}
             
loop();