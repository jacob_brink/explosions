var http = require("http");
var io = require("socket.io");
var url = require('url');
var fs = require('fs');

var server = http.createServer(function(request, response){
    
    var path = url.parse(request.url).pathname;
    
    switch(path){
        case '/':
            response.writeHead(200, {'Content-Type': 'text/html'});
            response.write('hello world');
            response.end();
            break;
        case '/index.html':
            fs.readFile(__dirname + path, function(error, data){
                if (error){
                    response.writeHead(404);
                    response.write("opps this doesn't exist - 404");
                    response.end();
                }
                else{
                    response.writeHead(200, {"Content-Type": "text"});
                    response.write(data, "utf8");
                    response.end();
                }
            });
            break;
        case '/game.html':
            fs.readFile(__dirname + path, function(error, data){
                if (error){
                    response.writeHead(404);
                    response.write("opps this doesn't exist - 404");
                    response.end();
                }
                else{
                    response.writeHead(200, {"Content-Type": "text"});
                    response.write(data, "utf8");
                    response.end();
                }
            });
            break;
        case '/socket.js':
            fs.readFile(__dirname + path, function(error, data){
                if (error){
                    response.writeHead(404);
                    response.write("opps this doesn't exist - 404");
                    response.end();
                }
                else{
                    response.writeHead(200, {"Content-Type": "text"});
                    response.write(data, "utf8");
                    response.end();
                }
            });
            break;
        case '/client-side-javascript/main.js':
            fs.readFile(__dirname + path, function(error, data){
                if (error){
                    response.writeHead(404);
                    response.write("opps this doesn't exist - 404");
                    response.end();
                }
                else{
                    response.writeHead(200, {"Content-Type": "text"});
                    response.write(data, "utf8");
                    response.end();
                }
                
            });
        break;
        case '/client-side-javascript/pixi.js':
            fs.readFile(__dirname + path, function(error, data){
                if (error){
                    response.writeHead(404);
                    response.write("opps this doesn't exist - 404");
                    response.end();
                }
                else{
                    response.writeHead(200, {"Content-Type": "text"});
                    response.write(data, "utf8");
                    response.end();
                }
                
            });
        break;
            case '/client-side-javascript/keyboard.js':
            fs.readFile(__dirname + path, function(error, data){
                if (error){
                    response.writeHead(404);
                    response.write("opps this doesn't exist - 404");
                    response.end();
                }
                else{
                    response.writeHead(200, {"Content-Type": "text"});
                    response.write(data, "utf8");
                    response.end();
                }
                
            });
        break;
            case '/client-side-javascript/missile.js':
            fs.readFile(__dirname + path, function(error, data){
                if (error){
                    response.writeHead(404);
                    response.write("opps this doesn't exist - 404");
                    response.end();
                }
                else{
                    response.writeHead(200, {"Content-Type": "text"});
                    response.write(data, "utf8");
                    response.end();
                }
                
            });
        break;
            
        case '/client-side-javascript/platform.js':
            fs.readFile(__dirname + path, function(error, data){
                if (error){
                    response.writeHead(404);
                    response.write("opps this doesn't exist - 404");
                    response.end();
                }
                else{
                    response.writeHead(200, {"Content-Type": "text"});
                    response.write(data, "utf8");
                    response.end();
                }
                
            });
        break;
            case '/client-side-javascript/rocket.js':
            fs.readFile(__dirname + path, function(error, data){
                if (error){
                    response.writeHead(404);
                    response.write("opps this doesn't exist - 404");
                    response.end();
                }
                else{
                    response.writeHead(200, {"Content-Type": "text"});
                    response.write(data, "utf8");
                    response.end();
                }
                
            });
        break;
        case '/jquery/jquery.js':
            fs.readFile(__dirname + path, function(error, data){
                if (error){
                    response.writeHead(404);
                    response.write("opps this doesn't exist - 404");
                    response.end();
                }
                else{
                    response.writeHead(200, {"Content-Type": "text"});
                    response.write(data, "utf8");
                    response.end();
                }
                
            });
        break;
        case '/client-side-javascript/index.js':
        fs.readFile(__dirname + path, function(error, data){
            if (error){
                response.writeHead(404);
                response.write("opps this doesn't exist - 404");
                response.end();
            }
            else{
                response.writeHead(200, {"Content-Type": "text"});
                response.write(data, "utf8");
                response.end();
            }
                
            });
        break;
        case '/css/index.css':
        fs.readFile(__dirname + path, function(error, data){
            if (error){
                 response.writeHead(404);
                 response.write("opps this doesn't exist - 404");
                 response.end();
            }
            else{
                 response.writeHead(200, {"Content-Type": "text/css"});
                 response.write(data, "utf8");
                 response.end();
                }
                
        });
        break;
        case '/client-side-javascript/pixi.input.js':
            fs.readFile(__dirname + path, function(error, data){
                if (error){
                    response.writeHead(404);
                    response.write("opps this doesn't exist - 404");
                    response.end();
                }
                else{
                    response.writeHead(200, {"Content-Type": "text"});
                    response.write(data, "utf8");
                    response.end();
                }
                
            });
        break;   
        case '/sprites/sprite_sheet.png':
            fs.readFile(__dirname + path, function(error, data){
                if (error){
                    response.writeHead(404);
                    response.write("opps this doesn't exist - 404");
                    response.end();
                }
                else{
                    response.writeHead(200, {"Content-Type": "image/png"});
                    response.write(data, "utf8");
                    response.end();
                }
                
            });
        break;
        case '/sprites/tilesheet.png':
            fs.readFile(__dirname + path, function(error, data){
                if (error){
                    response.writeHead(404);
                    response.write("dfdfdfdfdfdfd");
                    response.end();
                }
                else{
                    response.writeHead(200, {"Content-Type": "image/png"});
                    response.write(data, "utf8");
                    response.end();
                }
                
            });
        break;
        default:
            response.writeHead(404);
            response.write("opps this doesn't exist - 404");
            response.end();
            break;
    }
    
    
});

function setX(x){
    this.x = x;
}
function setY(y){
    this.y = y;
}
function setScore(score){
    this.score = score;
}
function setName(name){
    this.name = name;
}
function person(name){
    this.setX = setX;
    this.setY = setY;
    this.setScore = setScore;
    this.setName = name;
}

function update(){
    
}

function game( player1, player2){
    this.player1 = player1;
    this.player2 = player2;
    this.player2_data = "empty";
    this.player1_data = "empty";
    this.gameover = false;
    this.update = update;
    this.start = false;
    this.player1_ready = false;
    this.player2_ready = false;
    this.ready = false;
}


var player_count = 0;
var game_count = 0;
var games = new Array(100);
var player_one = new Array(100);
var player_two = new Array(100);

server.listen(8081);

// Console will print the message
console.log('Server running at http://127.0.0.1:8081/');

io.listen(server);
var listener = io.listen(server);

var broadcast1;

listener.sockets.on('connection', function(socket){

    broadcast1 = function broadcast(to_player, from_player_data){
        socket.emit(to_player, {'update': from_player_data});
    }
    
    socket.on('client-request', function(data){
        //name of client: and name of person who is to be invited to game
        console.log("THERE IS A REQUEST!!!");
        var split_message = data.invitation.split(":");
        var from_player = split_message[0];
        var to_player = split_message[1];
        socket.broadcast.emit(to_player, {'message': "invitation:" + from_player});
    });
    
    socket.on('confirmed-request', function(data){
        var split_message = data.message.split(":");
        var from_player = split_message[0];
        var to_player = split_message[1];
        console.log("A fasdfgame has started between " + from_player + " and " + to_player);
        
        game_count += 1;
        socket.broadcast.emit(to_player, {'message': "start:" + game_count});
        socket.broadcast.emit(from_player, {'message': "start:" + game_count});
            
        
        
        var player1 = new person(from_player);
        var player2 = new person(to_player);
        player_one[game_count - 1] = from_player;
        player_two[game_count - 1] = to_player;
        games[game_count - 1] = new game(player1,player2);
    });
    
    socket.on('start', function(data){
        var split_message = data.message.split(":");
        var player = split_message[0];
        var gameId = split_message[1];
        
        if(player == player_one[gameId - 1]){
           games[gameId - 1].player1_ready = true;
           console.log("player1 is ready");
        } else if(player == player_two[gameId - 1]) {
            games[gameId - 1].player2_ready = true;
            console.log("player 2 is ready");
        }
        
        if((games[gameId - 1].player1_ready == true) && (games[gameId - 1].player2_ready == true)){
            games[gameId - 1].ready = true;
            game_update();
        }
       
    });
    
    socket.on('reject-request', function(data){
        console.log("Game invitation REJECTED");
        var split_message = data.message.split(":");
        var from_player = split_message[0];
        var to_player = split_message[1];
        socket.broadcast.emit(from_player, {'startGame': 'false' + ':' + to_player});
    });
    socket.on('disconnect', function(){
        console.log("Someone has disconnected!");
    });
    socket.on('player_update', function(data){
        var split_message = data.message.split(":");
        //message should include game id and whether the player is one or two
        var gameId = split_message[0]
        var player_number = split_message[1];
        var i = 0;
        
        
        
    });

});
    
 function game_update(){
        var i = 0;
        while(i < game_count){
            
            games[i].update();
            broadcast1(games[i].player1, games[i].player2_data);
            broadcast1(games[i].player2, games[i].player1_data);
            console.log("Game is being updated!!!");
            i += 1;
        }
        setTimeout(game_update, 10);
    }   


